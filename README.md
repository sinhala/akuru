# අකුරු
පාසල් විෂය නිර්දේශයේ ඇතුළත් සිංහල අකුරුවල හැඩ අනුව සකසා ඇති අකුරු.

### බලපත්‍රය
මෙහි ඇතුළත් සියලු නිර්මාණ CC BY-NC-SA 4.0 බලපත්‍රය යටතේ නිකුත් කර ඇත.
<br>

Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
